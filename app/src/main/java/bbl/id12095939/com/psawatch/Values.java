/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package bbl.id12095939.com.psawatch;


public class Values {
    public static String[] receivers = {"Netflix","Hulu","Discovery","Optus","Telstra", "TPG", "Starbucks", "Steam", "GoogleAd", "Dropbox", "Skype"};
    public static String[] descriptions = {"Subscription","Payment","Prepaid","Loan"};
    public static String[] transaction_id = {"753849","392537","102254","992635","001231", "195844", "618398", "234150", "159735", "689001", "333551"};
    public static String[] amounts = {"11.99","23.65","17.25","10.00","31.90", "20.75", "12.80", "16.90", "9.99", "18.30", "5.00"};
    public static String[] important = {"true", "false"};
    public static String DOLLAR = "$";
    public static String MINUS = "-";
    public static String PLUS = "+";
    public static String DOT = ".";
    public static String PERCENT = "%";
    public static String[] quotes = {"Too many people spend money they earned..to buy things they don’t want..to impress people that they don’t like \n  –Will Rogers", "A wise person should have money in their head, but not in their heart. \n –Jonathan Swift", "Wealth consists not in having great possessions, but in having few wants. \n –Epictetus", "Everyday is a bank account, and time is our currency. No one is rich, no one is poor, we’ve got 24 hours each. \n –Christopher Rice", "It’s how you deal with failure that determines how you achieve success. \n –David Feherty", "An investment in knowledge pays the best interest. \n –Benjamin Franklin", "Money never made a man happy yet, nor will it. The more a man has, the more he wants. Instead of filling a vacuum, it makes one. \n  –Benjamin Franklin", "Happiness is not in the mere possession of money; it lies in the joy of achievement, in the thrill of creative effort. \n –Franklin D. Roosevelt", "Empty pockets never held anyone back. Only empty heads and empty hearts can do that. \n –Norman Vincent Peale", "If money is your hope for independence you will never have it. The only real security that a man will have in this world is a reserve of knowledge, experience, and ability. \n –Henry Ford", "Wealth is the ability to fully experience life. \n –Henry David Thoreau", "Every time you borrow money, you’re robbing your future self. \n –Nathan W. Morris", "The habit of saving is itself an education; it fosters every virtue, teaches self-denial, cultivates the sense of order, trains to forethought, and so broadens the mind. \n –T.T. Munger", "A successful man is one who can lay a firm foundation with the bricks others have thrown at him. \n –David Brinkley", "Screw it, Let’s do it! \n –Richard Branson"};
}
