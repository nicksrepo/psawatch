/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bbl.id12095939.com.psawatch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.wearable.view.CardFrame;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;

public class TransactionAdapter extends WearableListView.Adapter {
    //commit
    private final LayoutInflater mInflater;
    private Context mContext;

    public TransactionAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WearableListView.ViewHolder(
                mInflater.inflate(R.layout.card_view, null));
    }

    @Override
    public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
        final TextView receiver_view = (TextView) holder.itemView.findViewById(R.id.receiver_text);
        TextView amount_view = (TextView) holder.itemView.findViewById(R.id.amount_text);
        CardFrame cardBody = (CardFrame) holder.itemView.findViewById(R.id.card_body);
        receiver_view.setText(listItems.get(position).getReceiver());
        amount_view.setText("-$" + listItems.get(position).getAmount());
        holder.itemView.setTag(position);
        cardBody.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                if(receiver_view.getCurrentTextColor() == Color.parseColor("#FF3232")){
                    alertDialogBuilder.setMessage("Unsave this payment?");
                }else{
                    alertDialogBuilder.setMessage("Save this payment?");
                }

                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if(receiver_view.getCurrentTextColor() == Color.parseColor("#FF3232")){
                                    receiver_view.setTextColor(Color.parseColor("#212121"));
                                    notifyDataSetChanged();
                                }else{
                                    receiver_view.setTextColor(Color.parseColor("#FF3232"));
                                    notifyDataSetChanged();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    private static LinkedList<Transaction> listItems;
    static {
        listItems = new LinkedList<Transaction>();
        for (int i = 0; i < 30; i++) {
            listItems.add(new Transaction(Values.receivers[RandomGen.getRandom(Values.receivers.length,0)], Double.parseDouble(Values.amounts[RandomGen.getRandom(Values.amounts.length,0)]), "12/05/2016", Values.descriptions[RandomGen.getRandom(Values.descriptions.length, 0)], 1, Boolean.parseBoolean(Values.important[RandomGen.getRandomOneOfTwo()])));
        }
    }
}
