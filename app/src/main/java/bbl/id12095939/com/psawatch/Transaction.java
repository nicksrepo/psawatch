/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bbl.id12095939.com.psawatch;

public class Transaction {

    private String mReceiver;
    private double mAmount;
    private String mDate;
    private String mDescription;
    private int mID;
    private boolean mImportant;

    public Transaction(String mReceiver, double mAmount, String mDate, String mDescription, int mID, boolean important) {
        this.mReceiver = mReceiver;
        this.mAmount = mAmount;
        this.mDate = mDate;
        this.mDescription = mDescription;
        this.mID = mID;
        this.mImportant = important;

    }

    public String getReceiver() {
        return mReceiver;
    }

    public void setReceiver(String receiver) {
        mReceiver = receiver;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getID() {
        return mID;
    }

    public void setID(int ID) {
        this.mID = ID;
    }

    public boolean isImportant() {
        return mImportant;
    }

    public Boolean getImportantStatus(){
        return mImportant;
    }

    public void setImportant(boolean mImportant) {
        this.mImportant = mImportant;
    }
}
