/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bbl.id12095939.com.psawatch;

import java.util.Random;

/**
 * Class which contains functions related to producing of random data
 */
public class RandomGen {


    /**
     * Class which return random value which satisfies the range of passed parameters
     */
    public static int getRandom(int max, int min) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    /**
     * Class which returns random String time in format HH:MM where HH<20&&HH>9 and MM>9&&MM<60
     */
    public static String getRandomTime(){
        return 1+""+RandomGen.getRandom(9,0)+":"+RandomGen.getRandom(59,10);
    }

    /**
     * Class which return randomly 0 or 1
     */
    public static int getRandomOneOfTwo(){
        return (int)(Math.random() * 2);
    }



}