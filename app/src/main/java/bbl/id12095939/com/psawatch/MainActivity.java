/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bbl.id12095939.com.psawatch;

import android.app.Activity;
import android.media.Image;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class MainActivity extends Activity implements WearableListView.ClickListener {

    private WearableListView mListView;
    private ImageView mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        mButton = (ImageView) findViewById(R.id.menu_button);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mListView = (WearableListView) stub.findViewById(R.id.wearable_list_view);
                mListView.setAdapter(new TransactionAdapter(MainActivity.this));
                mListView.setClickListener(MainActivity.this);

            }
        });

    }

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {

    }


    @Override
    public void onTopEmptyRegionClick() {

    }
}
